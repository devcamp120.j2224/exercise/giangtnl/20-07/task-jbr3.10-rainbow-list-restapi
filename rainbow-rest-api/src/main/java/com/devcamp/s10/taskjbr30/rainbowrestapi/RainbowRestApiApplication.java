package com.devcamp.s10.taskjbr30.rainbowrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowRestApiApplication.class, args);
	}

}
