package com.devcamp.s10.taskjbr30.rainbowrestapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RainbowRestApi {
    @CrossOrigin
    @GetMapping("/rainbow")
    public ArrayList<String> rainbow() {
        ArrayList<String> color = new ArrayList<String>();
        color.add("red");
        color.add("orange");
        color.add("yellow");
        color.add("green");
        color.add("blue");
        color.add("indigo");
        color.add("violet");
        return color;
    }
 
}
